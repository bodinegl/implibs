// Requires hmac_sha1.nut 

class TwitterClient {
    consumerKey = null
    consumerSecret = null
    accessToken = null
    accessSecret = null
    
    baseUrl = "https://api.twitter.com/";
    
    constructor (_consumerKey, _consumerSecret, _accessToken, _accessSecret) {
        this.consumerKey = _consumerKey;
        this.consumerSecret = _consumerSecret;
        this.accessToken = _accessToken;
        this.accessSecret = _accessSecret;
    }
    
    function fmthex(s) {
        local h = ""
    	for(local i=0;i<s.len();i++) h+=format("%02x", s[i]);
    	return h
	}	
    
    function validChar(c) {
        if (c >= 48 && c <= 57) return true;    // numbers
        if (c >= 65 && c <= 90) return true;    // uppercase letter
        if (c >= 97 && c <= 122) return true;    // lowercase letter
        if (c == 46 || c == 45 || c == 95 || c == 126) return true;       // special characters
        
        return false;
    }
    
    function encode(str) {
        // thanks @beardedinventor for this fix
        local r = "";
        foreach(s in str){
            if (validChar(s)) r += s.tochar();
            else r += "%"+format("%00x", s).toupper();
        }
        return r;
    }
    
    function nonce() {
        local str = http.base64encode(math.rand())
        local r = "";
        foreach(s in str){
            if (validChar(s)) r += s.tochar();
        }
        return r;
    }
    
    function sign_hmac_sha1(key, str) {
        local sign = hmac_sha1(key,str)
        return http.base64encode(sign)
    }
    
    function post_oauth1(postUrl, headers, post) {
        
        local time = time()
        local nonce = nonce();
        
        local parm_string = http.urlencode({oauth_consumer_key=consumerKey})
        parm_string += "&"+http.urlencode({oauth_nonce=nonce})
        parm_string += "&"+http.urlencode({oauth_signature_method="HMAC-SHA1"})
        parm_string += "&"+http.urlencode({oauth_timestamp=time})
        parm_string += "&"+http.urlencode({oauth_token=accessToken})
        parm_string += "&"+http.urlencode({oauth_version="1.0"})
        parm_string += "&"+post
        
        local signature_string = "POST&"+encode(postUrl)+"&"+encode(parm_string)
        
        local key = encode(consumerSecret)+"&"+encode(accessSecret)
                
        local sha1 = encode(sign_hmac_sha1(key, signature_string))

        local auth_header = "OAuth "
        auth_header += "oauth_consumer_key=\""+consumerKey+"\", "
        auth_header += "oauth_nonce=\""+nonce+"\", "
        auth_header += "oauth_signature=\""+sha1+"\", "
        auth_header += "oauth_signature_method=\""+"HMAC-SHA1"+"\", "
        auth_header += "oauth_timestamp=\""+time+"\", "
        auth_header += "oauth_token=\""+accessToken+"\", "
        auth_header += "oauth_version=\"1.0\""
        
        local headers = { "Authorization": auth_header,
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
        };
        
        local response = http.post(postUrl, headers, post).sendsync();
        
        return response
    }
    
    function update_status(status) {
        local postUrl = baseUrl + "1.1/statuses/update.json";
    
        local headers = { };
        local post = "status="+encode(status);
        
        local response = post_oauth1(postUrl, headers, post)
        if (response && response.statuscode != 200) {
            server.log("Error updating_status tweet. HTTP Status Code " + response.statuscode);
            server.log(response.body);
            return null;
        }
    }

}

local _CONSUMER_KEY = "ConsumerKey" 
local _CONSUMER_SECRET = "ConsumerSecret"
local _ACCESS_TOKEN = "AccessToken"
local _ACCESS_SECRET = "AccessTokenSecret"
twitter <- TwitterClient(_CONSUMER_KEY, _CONSUMER_SECRET, _ACCESS_TOKEN, _ACCESS_SECRET);

twitter.update_status("#ALERT: this is a test message")
