function left_rotate(x, n) { 
    // this has to handle signed integers
    return (x << n) | (x >> (32 - n)) & ~((-1 >> n) << n);
}

function swap32(val) {
    return ((val & 0xFF) << 24) | ((val & 0xFF00) << 8) | ((val >>> 8) & 0xFF00) | ((val >>> 24) & 0xFF);
}
    
function sha1(message) {

    local h0 = 0x67452301;
    local h1 = 0xEFCDAB89;
    local h2 = 0x98BADCFE;
    local h3 = 0x10325476;
    local h4 = 0xC3D2E1F0;
    local mb=blob((message.len()+9+63) & ~63)
    
    local original_byte_len = message.len();
    local original_bit_len = original_byte_len * 8;
    
    foreach (val in message) {
        mb.writen(val, 'b');
    }

    mb.writen('\x80', 'b')
    
    local l = ((56 - (original_byte_len + 1)) & 63) & 63;
    while (l--) {
  		mb.writen('\x00', 'b')
	}
	
    mb.writen('\x00', 'i')
    mb.writen(swap32(original_bit_len), 'i')
    
    for (local i=0;i<mb.len();i+=64) {
        local w=[]; w.resize(80);

        for(local j=0;j<16;j++) {
            local s = i + j*4;
            mb.seek(s, 'b');
            w[j] = swap32(mb.readn('i'));
        }

        for(local j=16;j<80;j++) {
            w[j] = left_rotate(w[j-3] ^ w[j-8] ^ w[j-14] ^ w[j-16], 1);
        }
    
        local a = h0;
        local b = h1;
        local c = h2;
        local d = h3;
        local e = h4;
    
        for(local i=0;i<80;i+=1) {
            local f=0;
            local k=0;

            if (i>=0 && i<=19) {
                f = d ^ (b & (c ^ d));
                k = 0x5A827999;
            }
            else if (i>=20 && i<= 39) {
                f = b ^ c ^ d;
                k = 0x6ED9EBA1;
            }
            else if (i>=40 && i<= 59) {
                f = (b & c) | (b & d) | (c & d) ;
                k = 0x8F1BBCDC;
            }
            else if (i>=60 && i<= 79) {
                f = b ^ c ^ d;
                k = 0xCA62C1D6;
            }
            
            local _a=a
            local _b=b
            local _c=c
            local _d=d
            local _e=e
            local _f=f
            
            a = (left_rotate(_a, 5) + _f + _e + k + w[i]) & 0xffffffff;
            b = _a;
            c = left_rotate(_b, 30);
            d = _c;
            e = _d;
        }
    
        h0 = (h0 + a) & 0xffffffff
        h1 = (h1 + b) & 0xffffffff 
        h2 = (h2 + c) & 0xffffffff
        h3 = (h3 + d) & 0xffffffff
        h4 = (h4 + e) & 0xffffffff
    }
    
    local hash = blob(20);
    hash.writen(swap32(h0),'i');
    hash.writen(swap32(h1),'i');
    hash.writen(swap32(h2),'i');
    hash.writen(swap32(h3),'i');
    hash.writen(swap32(h4),'i');

    return hash;
}

function blobxor_x5c(text) {
    local len = text.len();
    local a = blob(len)
    for (local i = 0; i < len; i++) {
        a.writen(text[i] ^ 0x5c ,'b');
    }
    
    return a;
}

function blobxor_x36(text) {
    local len = text.len();
    local a = blob(len)
    for (local i = 0; i < len; i++) {
        a.writen(text[i] ^ 0x36 ,'b');
    }

    return a;
}

function blobconcat(a,b) {
	  local len = b.len();
	  for(local i=0; i<len; i++) {
	      a.writen(b[i],'b');
	  }
	  return a;
}

function blobpad(s,n) {
	  local b = blob(n);
	  	  
	  local len = s.len();	  
	  for(local i=0; i<len; i++) {
	      b.writen(s[i],'b');
	  }

	  for(local i=n-s.len(); i; i--) {
	     b.writen('\x00', 'b');	
 	  }
	  return b;
}

function hmac_sha1(key, message) {

    local _key;

    if ( key.len() > 64 ) {
        _key = blobpad(sha1(key),64);
    }
    else if ( key.len() <= 64 ) {
        _key = blobpad(key,64);
    }
    
    local _ok = blobxor_x5c(_key);
    local _ik= blobxor_x36(_key);
   
    return sha1(blobconcat(_ok, sha1(blobconcat(_ik, message))));
}

// helper function
function _printhex(s) {
    local h = "";
    for(local i=0;i<s.len();i++) h+=format("%02x", s[i]);
    return h;
}

// test suite.
function TestSuite() {
	print("test 1\n");
	local signature = "The quick brown fox jumps over the lazy dog, The quick brown fox jumps over the lazy dog, The quick brown fox jumps over the lazy dog";    
    local s = sha1(signature);    
	print(_printhex(s) + " = 00b6ebea50c2119c78a825887dd5ddf1869bb283\n");
	
	print("test 2\n");
	local key="test"
	local signature="this is a test"
	local h = hmac_sha1(key, signature);
	print(_printhex(h) + " = 366d21d032bcf1f07279054a1b160496682a6a15\n");
	
	// from https://dev.twitter.com/docs/auth/creating-signature
	local key="kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw&LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE"
	local signature="POST&https%3A%2F%2Fapi.twitter.com%2F1%2Fstatuses%2Fupdate.json&include_entities%3Dtrue%26oauth_consumer_key%3Dxvz1evFS4wEEPTGEFPHBog%26oauth_nonce%3DkYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1318622958%26oauth_token%3D370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb%26oauth_version%3D1.0%26status%3DHello%2520Ladies%2520%252B%2520Gentlemen%252C%2520a%2520signed%2520OAuth%2520request%2521"
	
	print("test 3\n");
	local s = sha1(key);
	print(_printhex(h) + " = 366d21d032bcf1f07279054a1b160496682a6a15\n");
	
	print("test 4\n");
	local s = sha1(signature);
	print(_printhex(s) + " = 996eeac5353ddfac330dc6562f2d6d491db6623d\n");
	
	print("test 5\n");
	local h = hmac_sha1(key, signature);
	print(_printhex(h) + " = b679c0af18f4e9c587ab8e200acd4E48a93f8cb6\n");
	
	local signature="POST&https%3a%2f%2fapi.twitter.com%2f1.1%2fstatuses%2fupdate.json&oauth_consumer_key%3dEzidwoLP3yT6aKP8ctCMJw%26oauth_nonce%3dKG51bGwgOiAweChuaWwpKQ%253d%253d%26oauth_signature_method%3dHMAC-SHA1%26oauth_timestamp%3d1378305621%26oauth_token%3d1503340872-kT3ErocioPTDnC42pP475oKE1743TTv3UKxcfcN%26oauth_version%3d1.0%26status%3d%2523ALERT%253a%2520this%2520is%2520a%2520test%2520message"
	local key="chzWTmYeW2BEhYYL6qLBVGPbFM6fh8MqF9jt1mk&noyWFfMubJpoYvrnXFszwFc8N8lGSMpNpVMeyZXDw"
	print("test 6\n");
	local h = hmac_sha1(key, signature);
	print(_printhex(h) + " = b5f6b72c58bc0146cbe61a498fd126d8547b91f8\n");

}
TestSuite();
